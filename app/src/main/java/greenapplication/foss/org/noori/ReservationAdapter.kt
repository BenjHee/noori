package greenapplication.foss.org.noori

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import java.text.SimpleDateFormat


class ReservationAdapter(private val context: Context, private val dataSource:List<Reservation>) : BaseAdapter() {
    private val inflater: LayoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

    //1
    override fun getCount(): Int {
        return dataSource.size
    }

    //2
    override fun getItem(position: Int): Any {
        return dataSource[position]
    }

    //3
    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    //4
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        // Get view for row item
        val rowView = inflater.inflate(R.layout.list_item_recent_reservation, parent, false)

        val dateTextView = rowView.findViewById(R.id.listItemRecentReservation_dateTextView) as TextView
        val startTextView = rowView.findViewById(R.id.listItemRecentReservation_startTextView) as TextView
        val endTextView = rowView.findViewById(R.id.listItemRecentReservation_endTextView) as TextView
        val placeTextView = rowView.findViewById(R.id.listItemRecentReservation_placeTextView) as TextView
        val nameTextView = rowView.findViewById(R.id.listItemRecentReservation_nameTextView) as TextView

        val reservation = getItem(position) as Reservation

        dateTextView.text = SimpleDateFormat("dd/MM/yyyy").format(reservation.start)
        startTextView.text = SimpleDateFormat("HH:mm").format(reservation.start)
        endTextView.text = SimpleDateFormat("HH:mm").format(reservation.end)
        placeTextView.text = reservation.place
        nameTextView.text = reservation.firstName + " " + reservation.lastName.uppercase()
        return rowView
    }
}