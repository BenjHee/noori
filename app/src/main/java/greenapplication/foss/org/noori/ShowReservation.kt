package greenapplication.foss.org.noori

import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.google.zxing.BarcodeFormat
import com.journeyapps.barcodescanner.BarcodeEncoder
import java.lang.Exception

class ShowReservation : AppCompatActivity() {
    private lateinit var reservationBox: io.objectbox.Box<Reservation>
    lateinit var reservation:Reservation

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_show_reservation)

        reservationBox = ObjectBox.store.boxFor(Reservation::class.java)

        val id = intent.extras!!.getLong(EXTRA_ID)
        reservation = reservationBox.get(id)


        val nameTextView = findViewById<TextView>(R.id.showReservation_nameTextView)
        val placeTextView = findViewById<TextView>(R.id.showReservation_placeTextView)
        val dateTextView = findViewById<TextView>(R.id.showReservation_dateTextView)
        val timeTextView = findViewById<TextView>(R.id.showReservation_timeTextView)
        val idTextView = findViewById<TextView>(R.id.showReservation_idTextView)
        val qrCodeImageView = findViewById<ImageView>(R.id.showReservation_qrCodeImageView)


        nameTextView.text = reservation.firstName+" "+reservation.lastName
        placeTextView.text = reservation.place
        dateTextView.text = java.text.SimpleDateFormat("dd/MM/yyyy").format(reservation.start)
        timeTextView.text = java.text.SimpleDateFormat("HH:mm").format(reservation.start) + " - " +java.text.SimpleDateFormat("HH:mm").format(reservation.end)
        idTextView.text = "Réservation numéro " + reservation.code

        try {
            val barcodeEncoder = BarcodeEncoder()
            val bitmap:Bitmap=barcodeEncoder.encodeBitmap(reservation.raw,BarcodeFormat.QR_CODE,800,800)
            qrCodeImageView.setImageBitmap(bitmap)
        }catch (e:Exception){
            Toast.makeText(this, "Could not generate QR code", Toast.LENGTH_LONG).show()
        }
    }

    companion object {
        const val EXTRA_ID = "0"

        fun newIntent(context: Context, reservation: Reservation): Intent {
            val detailIntent = Intent(context, ShowReservation::class.java)

            detailIntent.putExtra(EXTRA_ID,reservation.id)

            return detailIntent
        }
    }

}