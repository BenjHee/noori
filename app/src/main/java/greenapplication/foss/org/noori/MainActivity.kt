package greenapplication.foss.org.noori

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.zxing.integration.android.IntentIntegrator


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

    }
    fun startScanner(v: View){
        IntentIntegrator(this).initiateScan()
    }

    fun launchRecentReservationsActivity(v: View) {
        val intent = Intent(this,RecentReservation::class.java).apply {
        }
        startActivity(intent)
    }



    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        val result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data)
        if (result != null) {
            if (result.contents == null) {
                Toast.makeText(this, "Cancelled", Toast.LENGTH_LONG).show()
            } else {
                val reservation = Reservation.fromQrCode(result.contents)
                if (reservation == null){
                    Toast.makeText(this, "Invalid QR", Toast.LENGTH_LONG).show()
                    return
                }
                val reservationBox = ObjectBox.store.boxFor(Reservation::class.java)
                reservationBox.put(reservation)
                Toast.makeText(this, "Scanned: " + result.contents, Toast.LENGTH_LONG).show()
                return
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }
}