package greenapplication.foss.org.noori

import io.objectbox.annotation.Entity
import io.objectbox.annotation.Id
import java.text.SimpleDateFormat
import java.util.*

@Entity
data class Reservation(
    @Id(assignable = true)
    var id: Long = 0,
    var code: String = "",
    var firstName: String = "",
    var lastName: String= "",
    var place: String= "",
    var start: Date= Date(),
    var end: Date= Date(),
    var raw: String = "",
){

    companion object{
        fun fromQrCode(qrCodeContent:String):Reservation?{

            val reservation = Reservation()

            val content = qrCodeContent.split("#")
            if (content.size != 7) return null
            reservation.id = content[0].hashCode().toLong()
            reservation.code = content[0]
            reservation.place = content[1]
            reservation.firstName = content[2]
            reservation.lastName = content[3]

            val dateFormat = SimpleDateFormat("dd/MM/yyyy HH:mm")
            reservation.start = dateFormat.parse(content[4]+" "+content[5])
            reservation.end = dateFormat.parse(content[4]+" "+content[6])
            if(reservation.start == null ||
                    reservation.end == null) return null
            reservation.raw = qrCodeContent
            return reservation
        }
    }
}