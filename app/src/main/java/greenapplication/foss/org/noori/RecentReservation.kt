package greenapplication.foss.org.noori

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity

class RecentReservation : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recent_reservation)

        reservationBox = ObjectBox.store.boxFor(Reservation::class.java)
        listView = findViewById(R.id.RecentReservation_ListView)
        var recentReservation:List<Reservation>
        recentReservation = if (reservationBox.count() > 0){
            reservationBox.all.sortedByDescending{ reservation -> reservation.start }
        }else{
            ArrayList()
        }

        if (recentReservation.size > 5) recentReservation = recentReservation.subList(0,5-1)
        val adapter = ReservationAdapter(this, recentReservation)
        listView.adapter = adapter

        val context = this
        listView.setOnItemClickListener { _, _, position, _ ->
            val reservation = recentReservation[position]
            val showIntent = ShowReservation.newIntent(context,reservation)
            startActivity(showIntent)
        }



    }
    private lateinit var reservationBox: io.objectbox.Box<Reservation>
    private lateinit var listView: android.widget.ListView

}