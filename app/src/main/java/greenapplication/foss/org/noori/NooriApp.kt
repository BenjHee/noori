package greenapplication.foss.org.noori

import android.app.Application

class NooriApp : Application() {
    override fun onCreate() {
        super.onCreate()
        ObjectBox.init(this)
    }
}